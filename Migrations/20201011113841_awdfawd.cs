﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TalkToTalk.Migrations
{
    public partial class awdfawd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "7f9492fb3d0243598eb659ed99053ebf",
                column: "ConcurrencyStamp",
                value: "560179f6-8ed2-499d-9ac7-212466b1e4d8");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1c160da2-7d65-4c1f-a6a7-3c290af57ac5",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "f3be48ee-3975-4764-b375-d5ac0168b85d", "AQAAAAEAACcQAAAAEEd1+oUsNyfUEP60r/0UR5hzaWmmodWjhOMqtINpCw5Ii5CU/r7vCRFQYwkgz8GYCA==" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "7f9492fb3d0243598eb659ed99053ebf",
                column: "ConcurrencyStamp",
                value: "e15a8b5a-6e0f-48e0-b195-28b7d809c9e8");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1c160da2-7d65-4c1f-a6a7-3c290af57ac5",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "484243ce-d411-46c7-95b8-17366388d2fc", "AQAAAAEAACcQAAAAEJZMYOoOEnQRn/yqUf2j8DpBJX7rvJ+Cg1XlhvSqDdJ1ia/6Pl99FBkLu1R1Zbx1qA==" });
        }
    }
}
