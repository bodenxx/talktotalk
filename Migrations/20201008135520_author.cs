﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TalkToTalk.Migrations
{
    public partial class author : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AuthorId",
                table: "NewsItems",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "7f9492fb3d0243598eb659ed99053ebf",
                column: "ConcurrencyStamp",
                value: "c54f47b4-7cef-4e80-ae14-7e4001a81dd7");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1c160da2-7d65-4c1f-a6a7-3c290af57ac5",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "76c93365-8866-42dc-af89-0bf116feed5a", "AQAAAAEAACcQAAAAEDmOkXJChNniq3W8hD+4tprlCMoq7bKbTgxU64LYbJs2gfWVdwv+88vjhD4guJN8MA==" });

            migrationBuilder.CreateIndex(
                name: "IX_NewsItems_AuthorId",
                table: "NewsItems",
                column: "AuthorId");

            migrationBuilder.AddForeignKey(
                name: "FK_NewsItems_AspNetUsers_AuthorId",
                table: "NewsItems",
                column: "AuthorId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_NewsItems_AspNetUsers_AuthorId",
                table: "NewsItems");

            migrationBuilder.DropIndex(
                name: "IX_NewsItems_AuthorId",
                table: "NewsItems");

            migrationBuilder.DropColumn(
                name: "AuthorId",
                table: "NewsItems");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "7f9492fb3d0243598eb659ed99053ebf",
                column: "ConcurrencyStamp",
                value: "2228d2b8-089d-4f6e-b7d3-750b5b019c40");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1c160da2-7d65-4c1f-a6a7-3c290af57ac5",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "f79e066e-384f-4587-b0ed-a22d469ec869", "AQAAAAEAACcQAAAAEOjCF3W12nOmInjd39Us8smzRevfhnjeYnQwmcdiIk5wYY7flW2+0DTm1o8uYNax0w==" });
        }
    }
}
