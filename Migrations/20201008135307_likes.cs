﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TalkToTalk.Migrations
{
    public partial class likes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Likes",
                table: "NewsItems",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "7f9492fb3d0243598eb659ed99053ebf",
                column: "ConcurrencyStamp",
                value: "2228d2b8-089d-4f6e-b7d3-750b5b019c40");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1c160da2-7d65-4c1f-a6a7-3c290af57ac5",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "f79e066e-384f-4587-b0ed-a22d469ec869", "AQAAAAEAACcQAAAAEOjCF3W12nOmInjd39Us8smzRevfhnjeYnQwmcdiIk5wYY7flW2+0DTm1o8uYNax0w==" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Likes",
                table: "NewsItems");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "7f9492fb3d0243598eb659ed99053ebf",
                column: "ConcurrencyStamp",
                value: "c515e904-c911-4d00-b75a-4780c514c27f");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1c160da2-7d65-4c1f-a6a7-3c290af57ac5",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "8a8555cb-8433-46cd-8b62-a4fd4f01602e", "AQAAAAEAACcQAAAAEDFBrHeZ9u3SVSbsm4+aLTjshlJ6IWwe2MOGbDmKtMejG0xcqfY838Uv/TV0/ERl4A==" });
        }
    }
}
