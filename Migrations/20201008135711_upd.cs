﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TalkToTalk.Migrations
{
    public partial class upd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "7f9492fb3d0243598eb659ed99053ebf",
                column: "ConcurrencyStamp",
                value: "75ff44f2-30c3-446f-a632-fa8a7a360922");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1c160da2-7d65-4c1f-a6a7-3c290af57ac5",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "6ed3c576-887f-4b12-a115-8dde0088b186", "AQAAAAEAACcQAAAAEHr7BbRzMQeoPZragm7FU3Kvvh1D0OTvdRP64zZRwMpM2XYFOWDsMsh6nfXqw8RWJA==" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "7f9492fb3d0243598eb659ed99053ebf",
                column: "ConcurrencyStamp",
                value: "c54f47b4-7cef-4e80-ae14-7e4001a81dd7");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1c160da2-7d65-4c1f-a6a7-3c290af57ac5",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "76c93365-8866-42dc-af89-0bf116feed5a", "AQAAAAEAACcQAAAAEDmOkXJChNniq3W8hD+4tprlCMoq7bKbTgxU64LYbJs2gfWVdwv+88vjhD4guJN8MA==" });
        }
    }
}
