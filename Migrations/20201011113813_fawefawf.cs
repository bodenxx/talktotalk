﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TalkToTalk.Migrations
{
    public partial class fawefawf : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_NewsItems_AspNetUsers_AuthorId",
                table: "NewsItems");

            migrationBuilder.DropIndex(
                name: "IX_NewsItems_AuthorId",
                table: "NewsItems");

            migrationBuilder.AlterColumn<string>(
                name: "AuthorId",
                table: "NewsItems",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "7f9492fb3d0243598eb659ed99053ebf",
                column: "ConcurrencyStamp",
                value: "e15a8b5a-6e0f-48e0-b195-28b7d809c9e8");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1c160da2-7d65-4c1f-a6a7-3c290af57ac5",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "484243ce-d411-46c7-95b8-17366388d2fc", "AQAAAAEAACcQAAAAEJZMYOoOEnQRn/yqUf2j8DpBJX7rvJ+Cg1XlhvSqDdJ1ia/6Pl99FBkLu1R1Zbx1qA==" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "AuthorId",
                table: "NewsItems",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "7f9492fb3d0243598eb659ed99053ebf",
                column: "ConcurrencyStamp",
                value: "75ff44f2-30c3-446f-a632-fa8a7a360922");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1c160da2-7d65-4c1f-a6a7-3c290af57ac5",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "6ed3c576-887f-4b12-a115-8dde0088b186", "AQAAAAEAACcQAAAAEHr7BbRzMQeoPZragm7FU3Kvvh1D0OTvdRP64zZRwMpM2XYFOWDsMsh6nfXqw8RWJA==" });

            migrationBuilder.CreateIndex(
                name: "IX_NewsItems_AuthorId",
                table: "NewsItems",
                column: "AuthorId");

            migrationBuilder.AddForeignKey(
                name: "FK_NewsItems_AspNetUsers_AuthorId",
                table: "NewsItems",
                column: "AuthorId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
