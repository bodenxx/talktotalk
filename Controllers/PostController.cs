﻿using Microsoft.AspNetCore.Mvc;
using System;
using TalkToTalk.Domain;
using TalkToTalk.Domain.Entities;

namespace TalkToTalk.Controllers
{
    public class PostController : Controller
    {
        private readonly DataManager dataManager;

        public PostController(DataManager dataManager)
        {
            this.dataManager = dataManager;
        }

     
        public IActionResult Index(Guid id)
        {
            var entity = id == default ? new NewsItem() : dataManager.NewsItems.GetNewsItemById(id);
            entity.Author = dataManager.UserRepository.GetUserById(entity.AuthorId);
            return View(entity);
        }


        //TODO unique likes per user
        public IActionResult Like(Guid id)
        {
            var entity = id == default ? new NewsItem() : dataManager.NewsItems.GetNewsItemById(id);
            entity.Likes++;
            dataManager.NewsItems.SaveNewsItem(entity);
            return RedirectToAction("Index", "Post", new {id = id});
        }
    }
}