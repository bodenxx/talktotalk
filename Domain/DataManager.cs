﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TalkToTalk.Domain.Repositories.Abstract;

namespace TalkToTalk.Domain
{
    public class DataManager
    {
        public INewsItemsRepository NewsItems { get; set; }
        public IUserRepository UserRepository { get; set; }

        public DataManager(INewsItemsRepository newsItems, IUserRepository userRepository)
        {
            NewsItems = newsItems;
            UserRepository = userRepository;
        }
    }
}
