﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using TalkToTalk.Domain.Repositories.Abstract;

namespace TalkToTalk.Domain.Repositories.EntityFramework
{
    public class UserRepository : IUserRepository
    {
        private readonly AppDbContext _context;

        public UserRepository(AppDbContext context)
        {
            _context = context;
        }

        public IQueryable<IdentityUser> GetUsers()
        {
            return _context.Users;
        }

        public IdentityUser GetUserById(string id)
        {
            return _context.Users.FirstOrDefault(x => x.Id == id);
        }

        public void SaveUser(IdentityUser entity)
        {
            if (entity.Id == default)
                _context.Entry(entity).State = EntityState.Added;
            else
                _context.Entry(entity).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public void RemoveUser(string id)
        {
            _context.Users.Remove(new IdentityUser() {Id = id});
        }
    }
}
