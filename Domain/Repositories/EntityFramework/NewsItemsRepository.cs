﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TalkToTalk.Domain.Entities;
using TalkToTalk.Domain.Repositories.Abstract;

namespace TalkToTalk.Domain.Repositories.EntityFramework
{
    public class NewsItemsRepository : INewsItemsRepository
    {
        private readonly AppDbContext context;

        public NewsItemsRepository(AppDbContext context)
        {
            this.context = context;
        }

        public IQueryable<NewsItem> GetNewsItems()
        {
            return context.NewsItems.OrderByDescending(x=> x.DateAdded );
        }

        public IQueryable<NewsItem> GetTopFiveItems()
        {
            return context.NewsItems.OrderByDescending(x => x.Likes).Take(5);
        }

        public NewsItem GetNewsItemById(Guid id)
        {
            return context.NewsItems.FirstOrDefault(x => x.Id == id);
        }

        public void SaveNewsItem(NewsItem entity)
        {
            if (entity.Id == default)
                context.Entry(entity).State = EntityState.Added;
            else
                context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();
        }

        public void DeleteNewsItem(Guid id)
        {
            context.NewsItems.Remove(new NewsItem() {Id = id});
            context.SaveChanges();
        }
    }
}
