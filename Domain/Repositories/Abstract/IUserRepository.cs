﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace TalkToTalk.Domain.Repositories.Abstract
{
    public interface IUserRepository
    {
        IQueryable<IdentityUser> GetUsers();
        IdentityUser GetUserById(string id);
        void SaveUser(IdentityUser entity);
        void RemoveUser(string id);
    }
}
