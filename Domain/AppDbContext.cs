﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using TalkToTalk.Domain.Entities;

namespace TalkToTalk.Domain
{
    public class AppDbContext : IdentityDbContext<IdentityUser>
    {
        public  AppDbContext(DbContextOptions<AppDbContext> options) : base(options){}

        public DbSet<NewsItem> NewsItems { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            //Creating  admin role
            builder.Entity<IdentityRole>().HasData(new IdentityRole
            {
                Id = "7f9492fb3d0243598eb659ed99053ebf",
                Name = "admin",
                NormalizedName = "ADMIN"
            });
            //Creating admin user
            builder.Entity<IdentityUser>().HasData(new IdentityUser
            {
                Id = "1c160da2-7d65-4c1f-a6a7-3c290af57ac5",
                UserName = "admin",
                NormalizedUserName = "ADMIN",
                Email = "mymail@gmail.com",
                NormalizedEmail = "MYMAIL@GMAIL.COM",
                EmailConfirmed = true,
                PasswordHash = new PasswordHasher<IdentityUser>().HashPassword(null, "superpassword"),
                SecurityStamp = string.Empty
            });
            //Binding admin roe to new user
            builder.Entity<IdentityUserRole<string>>().HasData(new IdentityUserRole<string>
            {
                RoleId = "7f9492fb3d0243598eb659ed99053ebf",
                UserId = "1c160da2-7d65-4c1f-a6a7-3c290af57ac5"
            });
        }
    }
}
