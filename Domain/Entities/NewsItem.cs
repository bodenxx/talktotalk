﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace TalkToTalk.Domain.Entities
{
    public class NewsItem : EntityBase
    {
        [Required(ErrorMessage = "Заполните название статьи")]

        [Display(Name = "Название статьи")]
        public override string Title { get; set; }

        [Display(Name = "Краткое описание статьи")]
        public override string Subtitle { get; set; }

        [Display(Name = "Содержание статьи")]
        public override string Text { get; set; } = "Содержание заполняется администрацией";

        [Display(Name = "Титульная картинка")] 
        public override string TitleImagePath { get; set; }

        public int Likes { get; set; } = 0;

        public IdentityUser Author { get; set; }
        public string AuthorId { get; set; }
    }
}
