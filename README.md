# TalkToTalk

Blog developed with using such technologies:

- ASP.NET MVC
- HTML, JS, CSS
- EntityFramework
- Identity

Database: MS SQL (you may change connection settings inside appsettings.json)