#pragma checksum "K:\Projects\Study\TalkToTalk\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "5c1f54cbd7127898588d1d527fdce76c7055e789"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "K:\Projects\Study\TalkToTalk\Views\_ViewImports.cshtml"
using TalkToTalk;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "K:\Projects\Study\TalkToTalk\Views\_ViewImports.cshtml"
using TalkToTalk.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "K:\Projects\Study\TalkToTalk\Views\_ViewImports.cshtml"
using TalkToTalk.Domain.Entities;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "K:\Projects\Study\TalkToTalk\Views\_ViewImports.cshtml"
using TalkToTalk.Domain;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"5c1f54cbd7127898588d1d527fdce76c7055e789", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"6eeb5c318d529d43d79b2d53fc78581e74ff2351", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<DataManager>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 2 "K:\Projects\Study\TalkToTalk\Views\Home\Index.cshtml"
  
    ViewData["Title"] = "Home Page";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<div id=\"wrapper\">\r\n\r\n    <!-- Header -->\r\n    ");
#nullable restore
#line 10 "K:\Projects\Study\TalkToTalk\Views\Home\Index.cshtml"
Write(await Html.PartialAsync("HeaderPartial"));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n    <!-- Menu -->\r\n    ");
#nullable restore
#line 13 "K:\Projects\Study\TalkToTalk\Views\Home\Index.cshtml"
Write(await Html.PartialAsync("HeaderMenuPartial"));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n<!-- Main -->\r\n    <div id=\"main\">\r\n        \r\n");
#nullable restore
#line 18 "K:\Projects\Study\TalkToTalk\Views\Home\Index.cshtml"
         foreach (NewsItem item in Model.NewsItems.GetNewsItems())
        {
            item.Author = Model.UserRepository.GetUserById(item.AuthorId);
            

#line default
#line hidden
#nullable disable
#nullable restore
#line 21 "K:\Projects\Study\TalkToTalk\Views\Home\Index.cshtml"
       Write(await Html.PartialAsync("PostsPartial", item));

#line default
#line hidden
#nullable disable
#nullable restore
#line 21 "K:\Projects\Study\TalkToTalk\Views\Home\Index.cshtml"
                                                          
        }

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        <!-- Pagination -->\r\n        <ul class=\"actions pagination\">\r\n            <li><a");
            BeginWriteAttribute("href", " href=\"", 595, "\"", 602, 0);
            EndWriteAttribute();
            WriteLiteral(" class=\"button-white large previous\">Previous Page</a></li>\r\n            <li><a href=\"#\" class=\"button-white large next\">Next Page</a></li>\r\n        </ul>\r\n    </div>\r\n\r\n    <!-- Sidebar -->\r\n    ");
#nullable restore
#line 32 "K:\Projects\Study\TalkToTalk\Views\Home\Index.cshtml"
Write(await Html.PartialAsync("SidebarPartial", Model));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n</div>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<DataManager> Html { get; private set; }
    }
}
#pragma warning restore 1591
