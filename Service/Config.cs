﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TalkToTalk.Service
{
    public class Config
    {
        public static string ConnectionString { get; set; }
        public static string BlogName { get; set; }
        public static string YouTube { get; set; }
        public static string ContactMe { get; set; }
    }
}
